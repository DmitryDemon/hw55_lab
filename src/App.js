import React, { Component } from 'react';
import './App.css';
import IngredientsHandlers from "./components/IngredientsHandler/IngredientsHandlers";
import CreateBurger from "./components/BurgerIngredients/CreateBurger";



class App extends Component {


    state = {
      ingredients: [
          {name: 'Bacon', count: 0},
          {name: 'Cheese', count: 0},
          {name: 'Salad', count: 0},
          {name: 'Meat', count: 0},
      ],
        totalPrice:20
    };

    addIngridientHandler = (ingridient) => {
      let ingredients = [...this.state.ingredients];
      let id = ingredients.findIndex(ingredient => ingredient.name === ingridient.name);
        ingredients[id].count = ingredients[id].count + 1;

        let totalPrice = this.state.totalPrice;
        totalPrice += ingridient.price;

      this.setState({ingredients, totalPrice})
    };
    removeIngridientHandler = (ingridient) => {
        let ingredients = [...this.state.ingredients];
        let id = ingredients.findIndex(ingredient => ingredient.name === ingridient.name);
        ingredients[id].count = ingredients[id].count - 1;
        let totalPrice = this.state.totalPrice;
        totalPrice -= ingridient.price;
        this.setState({ingredients, totalPrice})
    };




  render() {

      return (
      <div className="App">

       <IngredientsHandlers rem={this.removeIngridientHandler} ingredients={this.state.ingredients} addClickHandler={this.addIngridientHandler}/>
      <CreateBurger ingredients={this.state.ingredients} totalPrice={this.state.totalPrice}/>


      </div>
    );
  }
}

export default App;
