import React from 'react';
import './CreateBurger.css';


const fiellds = (fiellingArr) => {
    const arrFiellds = [];

    for (let j = 0; j < fiellingArr.length; j++){
        for (let i = 0; i < fiellingArr[j].count; i++){
            arrFiellds.push(<div key={"" + i + "" + j}  className={fiellingArr[j].name}></div>)
        }
    }
    return arrFiellds;
};



const CreateBurger = props =>

    <div className="burg"><h2>Burger</h2>
        <div className='price'><p>Price : {props.totalPrice}</p></div>
        <div className='BreadTop'>
            <div className="Seeds1"></div>
            <div className="Seeds2"></div>
        </div>

        {fiellds(props.ingredients)}

        <div className='BreadBottom'></div>


    </div>;

export default CreateBurger;