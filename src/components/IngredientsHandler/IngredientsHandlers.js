import React from 'react';

    let meatImage = "https://cuginicafe.com/wp-content/uploads/2015/12/sj-burger-patty.png";
    let cheaseImage = "https://cheesesofeurope.com/sites/default/files/styles/large/public/bleu-d-auvergne-secret.png?itok=okxsj4k4";
    let saladImage = "https://gallery.yopriceville.com/var/albums/Free-Clipart-Pictures/Vegetables-PNG/Green_Salad_Lettuce_PNG_Picture.png?m=1434276659";
    let beconImage = "https://www.pngmart.com/files/3/Bacon-PNG-File.png";

    const ingredients = [
        {name: 'Bacon', price: 30, image: beconImage},
        {name: 'Cheese', price: 20, image: cheaseImage},
        {name: 'Salad', price: 5, image: saladImage},
        {name: 'Meat', price: 50, image: meatImage}
    ];

const getIngridientsHandlers = props =>

    <div className="ingr"><h2>Ingredients</h2>
        {ingredients.map((ingredient, key) =>
            <div key={key} className='wrapper'>
                <button onClick={() => props.addClickHandler(ingredient)} className='btn'><img className='butt' src={ingredient.image}
            alt={ingredient.name}/><p>{ingredient.name}</p>X - {props.ingredients[key].count}</button>
                {props.ingredients[key].count > 0? <button className='btnRemove' onClick={() => props.rem(ingredient)}>удалить</button>: null}
            </div>
            )
        }
    </div>;




export default getIngridientsHandlers;